package main

import "fmt"

var remainingTickets uint = 50

type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets uint
}

// var bookings = []string{}
var bookings = make([]UserData, 0)

func main() {
	conferenceName := "Go Conference"
	const conferenceTicket int = 50

	// fmt.Printf("conferenceTicket is %T, remainingTickets is %T, conferenceName is %T\n", conferenceTicket, remainingTickets, conferenceName)

	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available.\n", conferenceTicket, remainingTickets)
	fmt.Println("Get your tickets here to attend")

	for {
		var firstName string
		var lastName string
		var email string
		var userTickets uint

		// ask user for their name
		fmt.Println("enter your first name: ")
		fmt.Scan(&firstName)

		fmt.Println("enter lastName: ")
		fmt.Scan(&lastName)

		fmt.Println("enter email: ")
		fmt.Scan(&email)

		fmt.Println("enter number of tickets: ")
		fmt.Scan(&userTickets)

		remainingTickets -= userTickets

		// bookings[56] = firstName + "  " + lastName
		// bookings = apppend(bookings, firstName + "  " + lastName)

		// fmt.Printf("the whole array: %v\n", bookings)
		// fmt.Printf("the first value: %v\n", bookings[0])
		// fmt.Printf("the whole array %v\n", bookings)
		// fmt.Printf("slice type %T\n", bookings)
		// fmt.Printf("slice length: %v\n", len(bookings))

		// userTickets = 2
		fmt.Printf("thank you %v %v for booking %v tickets. you will recieve a confirmation email at %v\n", firstName, lastName, userTickets, email)
		fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceName)

		// fmt.Println(remainingTickets)
		// fmt.Println(&remainingTickets)

		// userName = "tom"
		// userTickets = 3
		// fmt.Println(userName)
		// fmt.Printf("user %v booked %v tickets.\n", userName, userTickets)

		// conferenceTickets = 30
		// fmt.Println(conferenceTickets)
	}

}

// func apppend(bookings []string, s string) {
// 	panic("unimplemented")
// }
// func newFunction(remainingTickets uint, userTickets int) uint {
// 	remainingTickets = remainingTickets - userTickets
// 	return remainingTickets

func bookTicket(userTickets uint, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - userTickets

	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: userTickets,
	}
	bookings = append(bookings, userData)
}
